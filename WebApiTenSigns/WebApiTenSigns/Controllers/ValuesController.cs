﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiTenSigns.Models;

namespace WebApiTenSigns.Controllers
{
    //[Authorize]
    public class ValuesController : ApiController
    {

        public List<UserModel> usuarios = new List<UserModel>();
        // GET api/values
        public IEnumerable<UserModel> Get()
        {
            
            UserModel usu = new UserModel();
            UserModel usu2 = new UserModel();
            usu.name = "juan";
            usu.email = "juan@asa";
            usu.password = "123";
            usu.userId = "juan";

            usu2.userId = "jmontes";
            usu2.name = "jhonatan";
            usu2.email = "j@montes.com";
            usu2.password = "123";

            usuarios.Add(usu);
            usuarios.Add(usu2);

            return usuarios;
                                        
        }

        // GET api/values/5
        public UserModel Get(int id)
        {
            UserModel usu = new UserModel();
            if (id == 1) {
                usu = usuarios[1];                    
            } else
                if (id == 2) {
                usu = usuarios[2];
            }
            return usu;
        }

        public int Login(UserModel Usuario)
        {
            UserModel usu = new UserModel();
            UserModel usu2 = new UserModel();
            usu.name = "juan";
            usu.email = "juan@asa";
            usu.password = "123";
            usu.userId = "juan";

            usu2.userId = "jmontes";
            usu2.name = "jhonatan";
            usu2.email = "j@montes.com";
            usu2.password = "123";

            usuarios.Add(usu);
            usuarios.Add(usu2);

            int valida = 0;
            try
            {
                if (Usuario.userId == usuarios[1].userId && Usuario.password == usuarios[1].password) { valida = 1; };
            }
            catch (Exception ex) {
                valida = -1;
                throw new Exception(ex.InnerException + ex.Message);
            };         
            
            return valida;
        }

        // POST api/values
        //public void Post([FromBody]string value)
        //{
        //    usuarios.Add(value);
        //}

        //// PUT api/values/5
        //public void Put(int id, [FromBody]string value)
        //{
        //    usuarios[id] = value;
        //}

        // DELETE api/values/5
        public void Delete(int id)
        {
            usuarios.RemoveAt(id);
        }
    }
}
