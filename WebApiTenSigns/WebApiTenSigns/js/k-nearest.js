(function () {
	
	window.KNN = {};
	
	KNN.Item = function (object) {
		for (var key in object) {
			if (object.hasOwnProperty(key)) {
				this[key] = object[key];
			}
		}
	};
	
	KNN.Item.prototype.measureDistances = function(edad_obj, sexo_obj,cat1_obj,cat2_obj,cat3_obj,cat4_obj) {
	
		var sexo_range = sexo_obj.max - sexo_obj.min;
		var edad_range  = edad_obj.max  - edad_obj.min;
		var cat1_range,cat2_range,cat3_range,cat4_range;
		
        if(this.cat1 != 0){
		 cat1_range  = cat1_obj.max  - cat1_obj.min;
		 cat2_range  = cat2_obj.max  - cat2_obj.min;
		 cat3_range  = cat3_obj.max  - cat3_obj.min;
		 cat4_range  = cat4_obj.max  - cat4_obj.min;
		 }

		for (var i in this.neighbors) {
			if (this.neighbors.hasOwnProperty(i)) {

				var neighbor = this.neighbors[i];

				var delta_sexo = neighbor.sexo - this.sexo;
				delta_sexo = (delta_sexo) / sexo_range;

				var delta_edad  = neighbor.edad  - this.edad;
				delta_edad = (delta_edad) / edad_range;

				if(this.cat1 != 0){
				var delta_cat1  = neighbor.cat1  - this.cat1;
				delta_cat1 = (delta_cat1) / cat1_range;

				var delta_cat2  = neighbor.cat2  - this.cat2;
				delta_cat2 = (delta_cat2) / cat2_range;

				var delta_cat3  = neighbor.cat3  - this.cat3;
				delta_cat3 = (delta_cat3) / cat3_range;

				var delta_cat4  = neighbor.cat4  - this.cat4;
				delta_cat4 = (delta_cat4) / cat4_range;
			           }

			    if(this.cat1 != 0){
				neighbor.distance = Math.sqrt( delta_sexo*delta_sexo + delta_edad*delta_edad + delta_cat1*delta_cat1 + delta_cat2*delta_cat2 + delta_cat3*delta_cat3 + delta_cat4*delta_cat4 );
				}else{
					console.log("entra igual a cero");
					neighbor.distance = Math.sqrt( delta_sexo*delta_sexo + delta_edad*delta_edad );
				}
			}
		}
	};
	
	KNN.Item.prototype.sortByDistance = function() {
		this.neighbors.sort(function (a, b) {
			return a.distance - b.distance;
		});
	};
	
	KNN.Item.prototype.guessType = function(k) {
	
		var types = {};

		for (var i in this.neighbors.slice(0, k)) {
		
			var neighbor = this.neighbors[i];

			if ( ! types[neighbor.type] ) {
				types[neighbor.type] = 0;
			}

			types[neighbor.type] += 1;
		}

		var guess = {type: false, count: 0};
		
		for (var type in types) {
			if (types[type] > guess.count) {
				guess.type = type;
				guess.count = types[type];
			}
		}

		this.guess = guess;
		$('#clasificacion').text("PERFIL DE CLASIFICACIÓN: "+guess.type);
		console.log(guess);

		return types;
	};

	KNN.ItemList = function (k) {
		this.nodes = [];
		this.k = k;
	};

	KNN.ItemList.prototype.add = function (node) {
		this.nodes.push(node);
	};

	KNN.ItemList.prototype.calculateRanges = function() {
		this.edades = {min: 1000000, max: 0};
		this.sexo = {min: 1000000, max: 0};

		this.cat1 = {min: 1000000, max: 0};
		this.cat2 = {min: 1000000, max: 0};
		this.cat3 = {min: 1000000, max: 0};
		this.cat4 = {min: 1000000, max: 0};
		
		for (var i in this.nodes) {
			if (this.nodes.hasOwnProperty(i)) {
			
				if (this.nodes[i].sexo < this.sexo.min) {
					this.sexo.min = this.nodes[i].sexo;
				}

				if (this.nodes[i].sexo > this.sexo.max) {
					this.sexo.max = this.nodes[i].sexo;
				}

				if (this.nodes[i].edad < this.edades.min) {
					this.edades.min = this.nodes[i].edad;
				}

				if (this.nodes[i].edad > this.edades.max) {
					this.edades.max = this.nodes[i].edad;
				}


                
                if (this.nodes[i].cat1 < this.cat1.min) {
					this.cat1.min = this.nodes[i].cat1;
				}

				if (this.nodes[i].cat1 > this.cat1.max) {
					this.cat1.max = this.nodes[i].cat1;
				}

				if (this.nodes[i].cat2 < this.cat2.min) {
					this.cat2.min = this.nodes[i].cat2;
				}

				if (this.nodes[i].cat2 > this.cat2.max) {
					this.cat2.max = this.nodes[i].cat2;
				}

				if (this.nodes[i].cat3 < this.cat3.min) {
					this.cat3.min = this.nodes[i].cat3;
				}

				if (this.nodes[i].cat3 > this.cat3.max) {
					this.cat3.max = this.nodes[i].cat3;
				}

				if (this.nodes[i].cat4 < this.cat4.min) {
					this.cat4.min = this.nodes[i].cat4;
				}

				if (this.nodes[i].cat4 > this.cat4.max) {
					this.cat4.max = this.nodes[i].cat4;
				}

			}
		}

	};
	
	KNN.ItemList.prototype.determineUnknown = function () {

		this.calculateRanges();

		/*
		 * Loop through our nodes and look for unknown types.
		 */
		for (var i in this.nodes) {
		
			if (this.nodes.hasOwnProperty(i)) {
			
				if ( ! this.nodes[i].type) {
					/*
					 * If the node is an unknown type, clone the nodes list and then measure distances.
					 */
					
					/* Clone nodes */
					this.nodes[i].neighbors = [];
					
					for (var j in this.nodes) {
						if ( ! this.nodes[j].type)
							continue;
						this.nodes[i].neighbors.push( new KNN.Item(this.nodes[j]) );
					}

					/* Measure distances */
					this.nodes[i].measureDistances(this.edades, this.sexo, this.cat1 , this.cat2 , this.cat3 , this.cat4);

					/* Sort by distance */
					this.nodes[i].sortByDistance();

					/* Guess type */
					this.type = this.nodes[i].guessType(this.k);

				}
			}
		}
	};
	
	/* KNN.ItemList.prototype.draw = function(canvas_id) {
	
		var rooms_range = this.sexo.max - this.sexo.min;
		var areas_range = this.edades.max - this.edades.min;

		var canvas = document.getElementById(canvas_id);
		var ctx = canvas.getContext("2d");
		var width = 800;
		var height = 800;
		ctx.clearRect(0,0,width, height);

		for (var i in this.nodes) {
			
			if (this.nodes.hasOwnProperty(i)) {
			
				ctx.save();

				switch (this.nodes[i].type) {
					case 'perfil1':
						ctx.fillStyle = 'red';						
						break;
					case 'perfil2':
						ctx.fillStyle = 'green';
						break;
					case 'perfil3':
						ctx.fillStyle = 'blue';
						break;
					default:
						ctx.fillStyle = '#666666';
				}

				var padding = 40;
				var x_shift_pct = (width  - padding) / width;
				var y_shift_pct = (height - padding) / height;

				var x = (this.nodes[i].sexo - this.sexo.min) * (width  / rooms_range) * x_shift_pct + (padding / 2);
				var y = (this.nodes[i].edad  - this.edades.min) * (height / areas_range) * y_shift_pct + (padding / 2);
				y = Math.abs(y - height);


				ctx.translate(x, y);
				ctx.beginPath();
				ctx.arc(0, 0, 5, 0, Math.PI*2, true);
				ctx.fill();
				ctx.closePath();
							

				if ( ! this.nodes[i].type ) {
					switch (this.nodes[i].guess.type) {
						case 'perfil1':
							ctx.strokeStyle = 'red';
							break;
						case 'perfil2':
							ctx.strokeStyle = 'green';
							break;
						case 'perfil3':
							ctx.strokeStyle = 'blue';
							break;
						default:
							ctx.strokeStyle = '#666666';
					}

					var radius = this.nodes[i].neighbors[this.k - 1].distance * width;
					radius *= x_shift_pct;
					ctx.beginPath();
					ctx.arc(0, 0, radius, 0, Math.PI*2, true);
					ctx.stroke();
					ctx.closePath();

				}

				ctx.restore();
			}
		}

	}; */
	
})();
