var getData = function () { 
	return $.getJSON("../../data/data.json"); 
};

var run = function (kInput) {

	var usuario = new KNN.ItemList(kInput);
	
	$.when(getData().done(function (json) {

		$.each(json.data, function (k,v) {
			usuario.add( new KNN.Item(v) );
		});
		
	}).then(function () {

		/*  
		cat1 = sol y playa
		cat2 = patrimonio cultural
		cat3 = aventura
		cat4 = gastronomico
		Masculino = 0
		Femenino = 1
		*/


		var random_sexo = $('#sexo').val(); //Math.round( Math.random() * 10 );
		var random_edad = $('#edad').val(); //Math.round( Math.random() * 2000 );

		console.log("sexo" + random_sexo + "Edad" + random_edad);
		
		var random_cat1 = 1;
		var random_cat2 = 1;
		var random_cat3 = 0;
		var random_cat4 = 0;
		
		usuario.add( new KNN.Item({sexo: random_sexo, edad: random_edad, cat1:random_cat1, cat2:random_cat2,cat3:random_cat3,cat4:random_cat4, type: false}) );
		usuario.determineUnknown();
		//usuario.draw("usuario");
		
	}).fail(function () {
	
		alert("Ocurrio un error !");
		
	}));
};

$("button#run").click(function () {
	var $input = $('#kInput').val();
	run($input); 
});